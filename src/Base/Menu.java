package Base;

import java.util.Scanner;

public class Menu {
	static byte option;
	static TV tv=new TV();

	public static void main(String[] args) {
		do {
			Menu();
		} while (option != 5);

		myUtility = new

				Bingo(); //Utilidad es un bingo
		myUtility.DoWork();// Hacer el trabajo de un bingo

		myUtility = new

				Parqueadero();//Utilidad es un parqueadero
		myUtility.DoWork();// Hacer el trabajo de un parqueadero
	}


	static void Menu() {
		Scanner input = new Scanner(System.in);

		System.out.println("Elija una opción \n1.Algoritmos \n2.Parqueadero \n3.Loteria \n4.Televisor \n5.Salir");
		option = input.nextByte();

		switch (option) {
			case 1:
				System.out.println("Bienvenido a algoritmos");//Esto mas adelante sera remplazado por los metodos que ejecutaran los respectivos programas.
				break;
			case 2:
				System.out.println("Bienvenido a parqueadero");//Esto mas adelante sera remplazado por los metodos que ejecutaran los respectivos programas.
				break;
			case 3:
				System.out.println("Bienvenido a la loteria");//Esto mas adelante sera remplazado por los metodos que ejecutaran los respectivos programas.
				break;
			case 4:
				tv.DoWork();
				break;
			case 5:
				System.out.println("Saliendo del programa");//Esto mas adelante sera remplazado por los metodos que ejecutaran los respectivos programas.
				break;
			default:
				System.out.println("Valor ingresado incorrecto");
				break;
		}
	}
}
